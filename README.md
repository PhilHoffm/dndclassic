# D&D Classic BECMI game system (unofficial) for Foundry VTT
All the features you need to play DnD Classic BECMI game system (unofficial) in Foundry VTT

## Installation
You can find this Foundry VTT game system within Foundry VTT in the system browser. You could also download the latest archive in the package folder, or use the manifest link below.\
https://gitlab.com/PhilHoffm/dndclassic/system.json

## License
This Foundry VTT system requires D&D Classic BECMI Core Rules that you can find [here](https://www.drivethrurpg.com/product/17171/DD-Rules-Cyclopedia-Basic).

D&D Classic BECMI is a trademark of Wizards of the Coast.\
The trademark and D&D Classic logo are used with permission of Wizards of the Coast, under license.

## Artwork
Weapon quality icons, and the Treasure chest are from [Rexxard](https://assetstore.unity.com/packages/2d/gui/icons/flat-skills-icons-82713).

## Contributions
Any feedback is deeply appreciated. Please issue [tickets](https://gitlab.com/PhilHoffm/dndclassic/-/boards).\
Feel free to grab a TO DO issue from the gitlab board. You can then do a merge request on the `development` branch.
